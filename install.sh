set -e
set -u

function install_brew() {
    /usr/bin/ruby -e curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install
}

function install_brew_packages() {
    brew bundle $HOME/.dotfiles/brewfile
}

function link_files() {
    oldDir=$HOME/.dotfiles_old/
    for file in ~/.dotfiles/*
        do
        if [[ -f $HOME/.$file ]]; then
            echo "$file found in $HOME \n moving to safe place!"
            mkdir $oldDir
            mv $file $oldDir
        else
            echo "Linking $file => $HOME/.$file"
            ln -s $file $HOME/.$file
        fi
    done
}

install_brew;
install_brew_packages;
link_files;
