;;; Numbero f lines to keep in log buffer.
(setq message-log-max 16384)

;;; Stupid emacs with garbage collect.
(setq gc-cons-threshold (* 2 1000 1000))

(setq gc-cons-percentage 0.6)

;; Don't load packages no matter how tempting it is.
(setq package-enable-at-startup nil)

;;; copy the variable.
(defvar file-name-handler-alist-old file-name-handler-alist)

;;; She a bigol regex.
(setq file-name-handler-alist nil)

;;; shh native comp
(setq native-comp-async-report-warnings-errors nil)

;;; Load path stuff.
(setq user-init-file (or load-file-name (buffer-file-name))
      user-emacs-directory (file-name-directory user-init-file))
(add-to-list 'load-path (concat user-emacs-directory "/lisp.d/"))

