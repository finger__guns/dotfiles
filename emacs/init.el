;;; init.el -- finger__guns
;;; Commentary:
;;; Emacs init for mostly OSX Use.
;;; Code:

;;; Set stuff back.
(add-hook 'after-init-hook
          `(lambda ()
             (setq file-name-handler-alist file-name-handler-alist-old
                   gc-cons-threshold 800000
                   gc-cons-percentage 0.1)
             (garbage-collect)) t)

(load "package-settings.el")

(load "appearance-settings.el")

(load "misc-settings.el")

(load "major-mode-settings.el")
