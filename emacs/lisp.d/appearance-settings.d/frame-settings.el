;;; frame-settings.el --- finger__guns
;;; Commentary:
;;; Code:

(use-package frame
  :config
  (blink-cursor-mode -1))

(use-package scroll-bar
  :config
  (horizontal-scroll-bar-mode -1)
  (scroll-bar-mode -1))

(use-package faces
  :init
  (set-face-attribute 'default nil :font "Envy Code R" :height 135)
		(set-face-attribute 'fixed-pitch nil :font "Iosevka" :weight 'regular :height 135)
		(set-face-attribute 'mode-line nil :font "Iosevka" :weight 'bold :height 130)
		(set-face-background 'cursor "#A9A9A9"))

(use-package hl-todo
  :straight t
  :hook (prog-mode . hl-todo-mode)
  :init
  (setq hl-todo-highlight-punctuation ":"
	hl-todo-keyword-faces
	`(("TODO"       warning bold)
	  ("FIXME"      error bold)
	  ("HACK"       font-lock-constant-face bold)
	  ("REVIEW"     font-lock-keyword-face bold)
	  ("NOTE"       success bold)
	  ("DEPRECATED" font-lock-doc-face bold))))

(use-package highlight-indent-guides
  :diminish highlight-indent-guides-mode
  :straight t
  :hook (prog-mode . highlight-indent-guides-mode)
  :config
  (setq highlight-indent-guides-method 'bitmap))

(use-package git-gutter
  :defer t
  :straight t
  :config
  (setq git-gutter:update-interval 0.02))

(use-package git-gutter-fringe
  :straight t
  :config
  (define-fringe-bitmap 'git-gutter-fr:added [224] nil nil '(center repeated))
  (define-fringe-bitmap 'git-gutter-fr:modified [224] nil nil '(center repeated))
  (define-fringe-bitmap 'git-gutter-fr:deleted [128 192 224 240] nil nil 'bottom))

(provide 'frame-settings)
;;; frame-settings.el ends here.
