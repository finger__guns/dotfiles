;;; window-settings.el --- finger_guns
;;; Commentary:
;;; How windows should look.
;;; Code:

(use-package window
  :config
  (setq display-buffer-alist
	'(("\\*\\(Flycheck\\|Flymake\\|Package-Lint\\|vc-git :\\).*"
           (display-buffer-in-side-window)
           (window-height . 0.16)
           (side . top)
           (slot . 0)
           (window-parameters . ((no-other-window . t))))
          ("^\\(\\*E?shell\\|vterm\\).*"
           (display-buffer-in-side-window)
           (window-height . 0.16)
           (side . bottom)
           (slot . 1))
          ("\\*Help.*"
           (display-buffer-in-side-window)
           (window-width . 0.20)       ; See the :hook
           (side . right)
           (slot . 0)
           (window-parameters . ((no-other-window . t)))))))

(use-package custom
  :init
  (add-to-list 'custom-theme-load-path (concat user-emacs-directory "/themes.d/"))
  :config
  (load-theme 'modus-vivendi t))

(provide 'window-settings)
;;; window-settings.el ends here.
