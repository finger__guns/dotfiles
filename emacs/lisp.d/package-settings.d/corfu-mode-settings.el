;;; corfu-mode-settings.el -- finger__guns
;;; Commentary:
;;; Completion engine.
;;; Code:

;; (use-package corfu
;;   :straight t
;;   :custom
;;   (corfu-cycle t)
;;   (corfu-auto t)
;;   (corfu-commit-predicate nil)
;;   (corfu-quit-at-boundary t)
;;   (corfu-quit-no-match t)
;;   (corfu-preview-current nil)
;;   (corfu-preselect-first nil)
;;   (corfu-echo-documentation nil)
;;   (corfu-scroll-margin 5)
;;   :bind
;;   (:map corfu-map
;; 	("C-j" . corfu-next)
;; 	("C-k" . corfu-previous)
;; 	("TAB" . corfu-insert)
;; 	("C-f" . corfu-insert))
;;   :hook ((prog-mode . corfu-mode)
;;          (shell-mode . corfu-mode)
;;          (eshell-mode . corfu-mode)))

(use-package orderless
  :straight t)

(use-package cape
  :straight t
  :init
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-tex)
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-keyword))

(use-package cape
  ;; Bind dedicated completion commands
  :bind (("C-c C-c" . completion-at-point)
         ("C-c C-f" . cape-file)
         ("C-c C-k" . cape-keyword)
         ("C-c C-s" . cape-symbol)
         ("C-c C-a" . cape-abbrev))
  :init
  ;; Add `completion-at-point-functions', used by `completion-at-point'.
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-tex)
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-keyword)
  (add-to-list 'completion-at-point-functions #'cape-sgml)
  (add-to-list 'completion-at-point-functions #'cape-rfc1345)
  (add-to-list 'completion-at-point-functions #'cape-abbrev)
  (add-to-list 'completion-at-point-functions #'cape-ispell)
  (add-to-list 'completion-at-point-functions #'cape-dict)
  (add-to-list 'completion-at-point-functions #'cape-symbol)
  (add-to-list 'completion-at-point-functions #'cape-line))
