;;; pyvenv-mode-settings.el --- finger__guns
;;; Commentary:
;;; Virtual environment-settings
;;; Code:

(use-package pyvenv
  :straight t
  :after (python)
  :hook (python-mode-hook . pyvenv-mode))

;; (use-package pyenv-mode
;;   :straight t)

(provide 'pyvenv-mode-settings)
;;; pyvenv-mode-settings.el ends here.
