;;; lsp-mode-settings.el --- finger__guns
;;; Commentary:
;;; Language server mode.
;;; Code:

;; (defun my/lsp-mode-setup-completion ()
;;     (setf (alist-get 'styles (alist-get 'lsp-capf completion-category-defaults))
;;           '(orderless))) ;; Configure orderless

(use-package lsp-mode
  :straight t
  :commands (lsp)
  :hook (prog-mode-hook . lsp)
  :bind((:map lsp-mode-map
              ([remap xref-find-apropos] . #'consult-lsp-symbols)))
  :delight "lsp"
  :config
  (setq lsp-keymap-prefix "C-c l")

  (setq lsp-enable-which-key-integration t)

  (setq read-process-output-max (* 1024 1024))
  (setq lsp-completion-provider :none)
  (setq lsp-headerline-breadcrumb-enable nil)

  (setq lsp-enable-links nil)
  (setq lsp-auto-guess-root t)

  (setq lsp-enable-xref t)
  (setq lsp-lens-enable nil)
  (setq lsp-completion-show-kind t)
  (setq lsp-modeline-diagnostics-scope :workspace)
  (setq lsp-intelephense-multi-root nil)
  (setq lsp-enable-file-watchers nil)
  (setq lsp-completion-show-kind t)
  (setq lsp-completion-show-detail t)
  (setq lsp-language-id-configuration
	'((rust-mode . "rust")
	  (python-mode . "python")
	  (typescript-mode . "typescript")
	  (js2-mode . "javascript")))
  (setq lsp-completion-enable t)
  (setq lsp-prefer-flymake nil))


(defun my/hide-frame-line-numbers (frame _window)
  "Hides line nunmbers from a specific frame in a winow."
  (select-frame frame)
  (display-line-numbers-mode -1))

(use-package lsp-ui
  :straight t
  :after (lsp-mode)
  :config
  (setq lsp-ui-sideline-enable nil
	lsp-ui-sideline-show-code-actions nil
	lsp-ui-sideline-enable t
	lsp-ui-sideline-show-diagnostics t)

  (setq lsp-ui-doc-enable t)
  (setq lsp-ui-doc-show-with-mouse nil)

  :bind (:map lsp-ui-mode-map
	      ([remap xref-find-definitions] . lsp-ui-peek-find-definitions)
	      ([remap xref-find-references] . lsp-ui-peek-find-references)
	      ("C-c u" . lsp-ui-imenu)))

(use-package dap-mode
  :straight t
  :custom
  (lsp-enable-dap-auto-configure t)
  :config
  (dap-ui-mode 1)
  :commands dap-debug)

(use-package lsp-pyright
  :straight t
  :custom
  (lsp-pyright-multi-root nil)
  :hook (python-mode . (lambda ()
                         (require 'lsp-pyright)
                         (lsp-deferred)))

  :init
  (when (executable-find "python3")
    (setq lsp-pyright-multi-root nil
	  lsp-pyright-auto-search-paths 't
	  lsp-pyright-python-executable-cmd "python3")))

(use-package tree-sitter
  :straight t
  :hook (prog-mode . tree-sitter-hl-mode)
  :init
  (tree-sitter-mode 1))

(use-package tree-sitter-langs
  :after tree-sitter
  :straight t)

(use-package treemacs
  :straight t
  :bind (
	 :map global-map ("C-c t" . treemacs)
	      (
	       :map lsp-mode-map ("C-c s" . lsp-treemacs-symbols))))


(use-package lsp-treemacs
  :straight t)

(provide 'lsp-mode-settings)
;;; lsp-mode-settings.el ends here.
