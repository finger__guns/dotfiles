;;; python-black-mode-settings.el --- finger__guns
;;; Commentary:
;;; Auto format the python i write please
;;; Code:

(use-package blacken
  :straight t
  :diminish blacken-mode
  :hook (python-mode . blacken-mode))

(provide 'python-black-mode-settings)
;;; python-black-mode-settings.el ends here.
