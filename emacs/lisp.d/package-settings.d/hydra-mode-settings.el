;;; hydra-mode-settings.el --- finger__guns
;; Commentary:
;;; Evil leader is being strange, lets try hydra.
;;; Code


(use-package hydra
  :straight t
  :bind (
	 ("C-c g" . hydra-lsp/body)
	 ("C-c w" . hydra-hs/body))
  :init
(defhydra hydra-lsp (:exit t :hint nil)
  "
 Buffer^^               Server^^                   Symbol
-------------------------------------------------------------------------------------
 [_f_] format           [_M-r_] restart            [_d_] declaration  [_i_] implementation  [_o_] documentation
 [_m_] imenu            [_S_]   shutdown           [_D_] definition   [_t_] type            [_r_] rename
 [_x_] execute action   [_M-s_] describe session   [_R_] references   [_s_] signature"
  ("d" lsp-find-declaration)
  ("D" lsp-ui-peek-find-definitions)
  ("R" lsp-ui-peek-find-references)
  ("i" lsp-ui-peek-find-implementation)
  ("t" lsp-find-type-definition)
  ("s" lsp-signature-help)
  ("o" lsp-describe-thing-at-point)
  ("r" lsp-rename)

  ("f" lsp-format-buffer)
  ("m" lsp-ui-imenu)
  ("x" lsp-execute-code-action)

  ("M-s" lsp-describe-session)
  ("M-r" lsp-restart-workspace)
  ("S" lsp-shutdown-workspace))
  (defhydra hydra-hs (:color blue)
    "
Hide^^            ^Show^            ^Toggle^    ^Navigation^
----------------------------------------------------------------
_ha_ hide all      _sa_ show all      _t_oggle    _n_ext line
_hb_ hide block    _sb_ show block                _p_revious line
_hl_ hide level

_SPC_ cancel
"
    ("sa" hs-show-all)
    ("ha" hs-hide-all)
    ("sb" hs-show-block)
    ("hb" hs-hide-block)
    ("t" hs-toggle-hiding)
    ("hl" hs-hide-level)
    ("n" forward-line)
    ("p" (forward-line -1))
    ("SPC" nil)))
