;;; flycheck-settings.el --- finger__guns
;;; Commentary:
;;; Syntax checking, mostly used with integration with lsp.
;;; Code:

(use-package flycheck
  :straight t
  :diminish flycheck-mode
  :hook (prog-mode . flycheck-mode)
  :config
  (setq flycheck-check-syntax-automatically '(save mode-enable)))

(provide 'flycheck-mode-settings)
;;; flycheck-mode-settings.el ends here
