;;; evil-mode-settings.el --- finger__guns
;;; Commentary:
;;; evil mode settings for emacs.
;;; Code:

(defvar-local was-hl-line-mode-on nil)
(defun hl-line-on-maybe ()  (if was-hl-line-mode-on (hl-line-mode +1)))
(defun hl-line-off-maybe () (if was-hl-line-mode-on (hl-line-mode -1)))
(add-hook 'hl-line-mode-hook 
	  (lambda () (if hl-line-mode (setq was-hl-line-mode-on t))))

(with-eval-after-load 'evil-maps
  (define-key evil-insert-state-map (kbd "C-n") nil)
  (define-key evil-insert-state-map (kbd "C-p") nil)
  (define-key evil-normal-state-map (kbd "C-n") nil)
  (define-key evil-normal-state-map (kbd "C-p") nil))

(use-package evil
  :straight t
  :bind ((:map evil-normal-state-map
              ("C-u" . evil-scroll-up)))
  :init
  (evil-mode 1)
  :config
  (setq evil-vsplit-window-right t)
  (setq evil-split-window-below t)
  (setq evil-shift-round nil)
  (setq evil-want-C-u-scroll t)
  (add-hook 'evil-insert-state-entry-hook 'hl-line-off-maybe)
  (add-hook 'evil-insert-state-exit-hook 'hl-line-on-maybe))

(use-package evil-surround
  :straight t
  :after evil
  :config
  (global-evil-surround-mode))

(use-package evil-commentary
  :diminish evil-commentary-mode
  :straight t
  :after evil
  :config
  (evil-commentary-mode))

(use-package evil-org
  :straight t
  :hook (org-mode . evil-org-mode))


(provide 'evil-mode-settings)
;;; evil-mode-settings.el ends here.
