;;; company-mode-settings.el --- finger__guns
;;; Commentary:
;;; Complete everything.
;;; Code:

(use-package company
  :straight t
  :diminish company-mode
  :hook (prog-mode . company-mode)
  :bind ((:map company-active-map
               ("M-/" . company-complete)
               ("C-n" . company-select-next)
               ("C-p" . company-select-previous)))
  :config
  (setq company-tooltip-align-annotations t
        company-dabbrev-downcase nil
        company-tooltip-limit 24
        company-idle-delay 0
        company-echo-delay 0
        company-minimum-prefix-length 1
        company-require-match nil
        company-dabbrev-ignore-case nil
        company-dabbrev-downcase nil)
  (push 'company-capf company-backends))


(use-package company-tabnine
  :straight t
  :after company-mode
  :init
  (add-to-list 'company-backends #'company-tabnine))

;;; company-mode-settings.el ends here
(provide 'company-mode-settings)
