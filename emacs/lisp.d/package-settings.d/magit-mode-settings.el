;;; magit-mode-settings.el --- finger__guns
;;; Commentary:
;;; The Best git interface.
;;; Code:

(use-package magit
  :straight t
  :bind (("C-x g" . magit-status))
  :bind (:map magit-mode-map
              ("U" . magit-unstage-all)
              ("M-h") ("M-s") ("M-m") ("M-w"))
  :bind (:map magit-file-section-map ("<C-return>"))
  :bind (:map magit-hunk-section-map ("<C-return>")))
(provide 'magit-mode-settings)
;;; magit-mode-settings.el ends here
