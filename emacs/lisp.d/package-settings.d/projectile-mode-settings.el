;;; projectile-mode-settings.el --- finger__guns
;;; Commentary:
;;; Manage projects.
;;; Code:

(use-package projectile
  :straight t
  :delight '(:eval (concat " " (projectile-project-name)))
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  (setq projectile-indexing-method 'hybrid)
  (setq projectile-sort-order 'recently-active)
  (setq projectile-enable-caching nil)
  (setq projectile-switch-project-action #'projectile-dired)
  :config
  (projectile-mode 1))


(provide 'projectile-mode-settings)
;;; projectile-mode-settings.el ends here.
