;;; package-settings.el --- finger__guns
;;; Commentary:
;;; Packages using straight.el - https://github.com/raxod502/straight.el
;;; Code:

;;; Bootstrap straight.el
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)
(setq use-package-verbose t)


(use-package diminish
  :straight t)

(use-package which-key
  :diminish which-key-mode
  :straight t
  :init
  (setq which-key-popup-type 'minibuffer)
  (setq which-key-idle-delay 10000)
  (setq which-key-idle-secondary-delay 0.05)
  :config
  (which-key-mode 1))

(use-package exec-path-from-shell
  :straight t
  :config
  (exec-path-from-shell-copy-env "WORKON_HOME")
  (exec-path-from-shell-initialize))

(load "package-settings.d/hydra-mode-settings.el")
(load "package-settings.d/evil-mode-settings.el")
(load "package-settings.d/company-mode-settings.el")
(load "package-settings.d/corfu-mode-settings.el")
(load "package-settings.d/lsp-mode-settings.el")
(load "package-settings.d/projectile-mode-settings.el")
(load "package-settings.d/magit-mode-settings.el")
(load "package-settings.d/flycheck-mode-settings.el")
(load "package-settings.d/embark-mode-settings.el")
(load "package-settings.d/pyvenv-mode-settings.el")
(load "package-settings.d/python-black-mode-settings.el")

(provide 'package-settings)
