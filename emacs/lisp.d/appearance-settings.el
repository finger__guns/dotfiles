;;; appearance-settings.el --- finger__guns
;;; Commentary:
;;; Code:

;; Turn off all the display things globally, we can turn them on for
;; modes that actually make use of them later.
(load "appearance-settings.d/frame-settings.el")
(load "appearance-settings.d/window-settings.el")

(add-to-list 'default-frame-alist '(inhibit-double-buffering . t))

(tool-bar-mode -1)
(menu-bar-mode -1)
(set-fringe-mode 15)


;; Go big or go home.
(add-to-list 'default-frame-alist '(fullscreen . maximized))

(use-package custom
  :config
  ;; Show me some relative line numbers
  (setq-default display-line-numbers 'relative
		display-line-numbers-width 3))

(use-package display-fill-column-indicator
  :hook (prog-mode-hook . #'display-fill-column-indicator-mode)
  :init
  (setq global-display-fill-column-indicator-mode t)
  :config
  (setq display-fill-column-indicator t)
  (setq display-fill-column-indicator-character ?\N{U+2506}))




;;; Shut up.
(setq ring-bell-function 'ignore)

(provide 'appearance-settings)
;;; appearance-settings.el ends here.
