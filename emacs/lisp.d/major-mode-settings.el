;;; major-mode-settings.el --- finger__guns
;;; Commentary:
;;; Code:


(load "major-mode-settings.d/text-mode-settings.el")
(load "major-mode-settings.d/prog-mode-settings.el")
(load "major-mode-settings.d/python-mode-settings.el")
(load "major-mode-settings.d/yaml-mode-settings.el")
(load "major-mode-settings.d/go-mode-settings.el")
(load "major-mode-settings.d/rust-mode-settings.el")

(provide 'major-mode-settings)
;;; major-mode-settings.el ends here.
