;;; misc-settings.el --- finger__guns
;;; Commentary:
;;; Just a bunch of settings that are default for me.
;;; Code:

;; load .dir-locals.el by default
(setq-default enable-local-variables t)

;; Stop warning me about every god damn thing, this is really useful for lsp things, should just re write that in lsp.el to set the default error level to be higher.
(setq warning-minimum-level :emergency)
(setq warning-suppress-log-types `((comp)))

;; ;; Go home silly emacs, You're drunk. This can also be done with env vars.
;; (setq default-directory "~/")

;; Stop recentf from being flooded.
(setq recentf-max-saved-items 2048
      recentf-exclude '("/tmp/" "/ssh:" "/sudo:"
                        "recentf$" "company-statistics-cache\\.el$" "/TAGS$"
                        "/GTAGS$" "/GRAGS$" "/GPATH$"
                        "\\.mkv$" "\\.mp[34]$" "\\.avi$"
                        "\\.pdf$" "\\.docx?$" "\\.xlsx?$"
                        "\\.sub$" "\\.srt$" "\\.ass$"
                        "~/.config/emacs/**/*.el"))

;; Garbage collect whenever the window loses focus.
(add-hook 'focus-out-hook #'garbage-collect)

;; Don't open with splits
(add-hook 'window-setup-hook #'delete-other-windows)

;; Tell emacs where to put the custom/autoset settings, this stops init.el being flooded.
(setq custom-file (concat user-emacs-directory "/custom.el"))

;; Just follow shit.
(setq vc-follow-symlinks t)

;; Delete items to the trash.
(setq trash-directory "~/.Trash"
      delete-by-moving-to-trash t)

;; Always use x clipboard.
(setq  x-select-enable-clipboard t)

;; Let's not complain about big files.
(setq large-file-warning-threshold nil)

;; Avoid the end of buffer error.
(setq next-line-add-newlines nil)

;; Always try and load the newer version of the file.
(setq load-prefer-newer t)

;; Load the tool tip quicker and nicer.
(setq tooltip-delay 1.5)

;; Don't shorten names or anything.
(setq find-file-use-truenames nil
      find-file-compare-truenames t)

;; Complete what can be and don't complain about it!
(setq minibuffer-max-depth nil
      minibuffer-comfirm-incomplete t)

;; Kill everything.
(setq  kill-whole-line t
       kill-read-only-ok t)

;; Don't start up with a fancy slash screen, just give me org with a message.
(setq inhibit-splash-screen t
      initial-scratch-message "* Welcome back to Emacs"
      initial-major-mode 'org-mode)

;; Store all backup and autosave files in the tmp dir.
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;; Go quick but be quite about it.
(setq echo-keystrokes 0.1
      use-dialog-box nil
      visible-bell nil)

(when (eq system-type 'darwin)
  ;; use all the special keys on the mac keyboard
  (setq mac-option-modifier 'super
        ns-function-modifier nil
        mac-right-command-modifier 'super
        mac-right-option-modifier 'meta
        mac-command-modifier 'meta))

(when (executable-find "hunspell")
  (setq-default ispell-program-name "hunspell")
  (setq ispell-really-hunspell t)
  (setq ispell-dictionary "en_AU"))

(provide 'misc-settings)
;;; misc-settings.el ends here
