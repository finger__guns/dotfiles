;;; go-mode-settings.el --- finger__guns
;;; Commentary:
;;; Go Mode.
;;; Code:


(use-package go-mode
  :straight t
  :mode ("\\.go\\'" . go-mode))
