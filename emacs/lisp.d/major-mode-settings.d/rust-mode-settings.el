;;; rust-mode-settings.el --- finger__guns
;;; Commentary:
;;; Rust mode
;;; Code:

(use-package rust-mode
  :straight t
  :mode ("\\.rs\\'" . rust-mode)
  :hook (rust-mode . lsp)
  :custom
  (lsp-rust-analyzer-cargo-watch-command "clippy")
  (lsp-eldoc-render-all t)
  (lsp-idle-delay 0.6)
  (lsp-rust-analyzer-server-display-inlay-hints t)
  :config
  (setq rust-format-on-save t))

;; (use-package rustic
;;   :straight t
;;   :mode ("\\.rs\\'" . rustic-mode)
;;   :init
;;   (setq rustic-analyzer-command '("~/.cargo/bin/rust-analyzer"))
;;   :config
;;   (setq rustic-format-on-save t))

(use-package cargo
  :straight t
  :hooks (rust-mode-hook . cargo-minor-mode))

(use-package toml-mode
  :straight t
  :mode ("\\.toml\\'" . toml-mode))

;;; rust-mode-settings.el ends here.
