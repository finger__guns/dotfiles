;;; yaml-mode-settings.el --- finger__guns
;;; Commentary:
;;; Yaml is usefull for pipelines and what not.
;;; Code:

(use-package yaml-mode
  :mode ("\\.yml\\'" . yaml-mode)
  :straight t)
;;; yaml-mode-settings.el ends here.
(provide 'yaml-mode-settings)
