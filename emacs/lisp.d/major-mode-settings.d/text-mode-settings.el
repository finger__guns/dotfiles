;;; text-mode-settings.el --- finger__guns
;;; Code:
;;; Parent of all text modes
;;; Commentary:

(use-package text
  :config
      (setq fill-column 150)
      (flyspell-mode 1))

(provide 'text-mode-settings)
;;; text-mode-settings.el ends here.
