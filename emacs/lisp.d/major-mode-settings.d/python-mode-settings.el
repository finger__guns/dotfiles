;;; python-mode-settings.el --- finger__guns
;;; Commentary:
;;; How python files should act.
;;; Code:


(use-package python
  :custom
  (dap-python-executable "python3")
  (dap-python-debugger 'debugpy)
  (toggle-debug-on-error)
  :config
  (require 'dap-python)
  (setq python-indent-offset 4)
  (setq dap-python--pyenv-executable-find t)
  (pyvenv-mode 1)
  )

(provide 'python-mode-settings)
;;; python-mode-settings.el ends here.
