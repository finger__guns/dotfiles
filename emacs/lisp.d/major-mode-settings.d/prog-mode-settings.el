;;; prog-mode-settings.el --- finger__guns
;;; Commentary:
;;; How all programming modes should act
;;; Code:


(use-package prog
  :hook (before-save-hook . whitespace-cleanup)
  :init
  (setq indent-tabs-mode nil
            tab-width 2
            c-basic-indent 2
            c-basic-offset 2
            evil-shift-width 4)
  (setq fill-column 110)
  (electric-indent-mode 1)
  (hl-line-mode 1)
  (hs-minor-mode 1)
  ;; Show matching parens.
  (show-paren-mode 1)
  (flyspell-prog-mode)
  (setq display-fill-column-indicator-mode t)
)


(use-package eldoc
  :diminish eldoc-mode t)
