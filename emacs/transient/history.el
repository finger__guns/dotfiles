((magit-blame
  ("-w"))
 (magit-commit nil)
 (magit-dispatch nil)
 (magit-fetch nil)
 (magit-pull
  ("--ff-only")
  nil)
 (magit-push nil)
 (magit-submodule nil))
