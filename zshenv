export PATH=/usr/local/bin:$PATH
export PATH=$HOME/.local/bin/:$PATH

export KEYTIMEOUT=1

export EDITOR="vim"

export FZF_DEFAULT_COMMAND='rg --files --hidden'

export NVM_DIR="$HOME/.nvm"
  [ -s "/usr/local/opt/nvm/nvm.sh" ] && . "/usr/local/opt/nvm/nvm.sh"  # This loads nvm
  [ -s "/usr/local/opt/nvm/etc/bash_completion.d/nvm" ] && . "/usr/local/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion

export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
export ANDROID_HOME=/Users/$USER/Library/Android/sdk
export PATH=${PATH}:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools
export DYLD_LIBRARY_PATH=/usr/local/Cellar/openssl@1.1/1.1.1g/lib

# The next line enables shell command completion for gcloud.
if [ -f '/Users/finger__guns/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/finger__guns/google-cloud-sdk/completion.zsh.inc'; fi

export PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
export PATH="/usr/local/opt/openssl@3/bin:$PATH"


export FZF_DEFAULT_OPTS='--height 40% --layout=reverse --border'
export FZF_DEFAULT_COMMAND='rg --files --no-ignore-vcs --hidden'
if [ -e /Users/finger__guns/.nix-profile/etc/profile.d/nix.sh ]; then . /Users/finger__guns/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer

export VIRTUALENVWRAPPER_PYTHON=/opt/homebrew/bin/python3
export WORKON_HOME=$HOME/.virtualenvs

# export GOPATH="$(go env GOPATH)"
# export PATH="${PATH}:${GOPATH}/bin"
# . "$HOME/.cargo/env"
