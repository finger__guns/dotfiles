# Dotfiles.
A collection of configuration files used for, well configuration.
This repo is split into multiple files and directories.

## Editors
Emacs is my go to editor, all the config and what not is stored in the emacs.d
directory all that fun stuff. There is a vimrc and what not, coz in shell or ssh
or what not vim is still the quickest.

## Shell
For a default shell, my go to is zsh just for all the nice things it gives you.
